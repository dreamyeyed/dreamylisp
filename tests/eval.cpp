#include <sstream>
#include <stdexcept>

#define BOOST_TEST_MODULE eval
#include <boost/test/included/unit_test.hpp>

#include "eval.hpp"
#include "parser.hpp"
#include "tokenizer.hpp"
#include "value.hpp"

using namespace dlisp;

Value add(const std::vector<Value>& numbers)
{
    double total = 0;
    for (const auto& val : numbers) {
        if (const double *num = boost::get<double>(&val.value)) {
            total += *num;
        } else {
            throw std::runtime_error("Invalid number");
        }
    }
    return Value{total};
}

Value equal(const std::vector<Value>& values)
{
    if (values.size() <= 1) {
        return Value{true};
    }

    bool first_read = false;
    double first;

    for (const auto& val : values) {
        if (const double *num = boost::get<double>(&val.value)) {
            if (first_read && *num != first) {
                return Value{false};
            } else if (!first_read) {
                first = *num;
            }
        }
    }

    return Value{true};
}

Form parse_code(const std::string& code)
{
    std::istringstream ss(code);

    Tokenizer tokenizer(ss);
    std::vector<Token> tokens = tokenizer.tokenize();

    Parser parser(tokens);
    return parser.parse();
}

BOOST_AUTO_TEST_CASE(add_numbers)
{
    std::shared_ptr<Env> env(new Env(nullptr));
    env->set("+", Value{add});

    std::string code = "(+ 1 2 3 4 5)";
    Form nodes = parse_code(code);

    Value result = eval(*nodes[0], env);
    if (double *d = boost::get<double>(&result.value)) {
        std::cout << code << " -> " << *d << std::endl;
        BOOST_CHECK(*d == 15);
    } else {
        BOOST_CHECK("incorrect result type" && false);
    }
}

BOOST_AUTO_TEST_CASE(if_expression)
{
    std::shared_ptr<Env> env(new Env(nullptr));
    env->set("+", Value{add});
    env->set("=", Value{equal});

    std::string code(
        "(if (= (+ 2 3 4) 9)\n"
        "  (if true\n"
        "    \"true\"\n"
        "    \"false\"))"
    );
    Form nodes = parse_code(code);

    Value result = eval(*nodes[0], env);
    if (const std::string *s = boost::get<std::string>(&result.value)) {
        std::cout << *s << std::endl;
        BOOST_CHECK(*s == "true");
    } else {
        BOOST_CHECK("incorrect result type" && false);
    }
}

BOOST_AUTO_TEST_CASE(custom_function)
{
    std::shared_ptr<Env> env(new Env(nullptr));
    env->set("+", Value{add});

    std::string code(
        "(def adder\n"
        "  (fn (x)\n"
        "    (fn (y)\n"
        "      (+ x y))))\n"
        "\n"
        "(def add-five (adder 5))\n"
        "\n"
        "(add-five 10)"
    );
    Form nodes = parse_code(code);

    Value result;
    for (size_t i = 0; i < nodes.size(); ++i) {
        if (i < nodes.size() - 1) {
            eval(*nodes[i], env);
        } else {
            result = eval(*nodes[i], env);
        }
    }
    if (const double *d = boost::get<double>(&result.value)) {
        std::cout << "5 + 10 = " << *d << std::endl;
        BOOST_CHECK(*d == 15);
    } else {
        BOOST_CHECK("incorrect result type" && false);
    }
}
