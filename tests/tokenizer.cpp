#include <algorithm>
#include <sstream>

#include <boost/test/included/unit_test.hpp>
#include <boost/test/parameterized_test.hpp>
using namespace boost::unit_test;

#include "tokenizer.hpp"
using namespace dlisp;

struct TestParameters
{
    // input given to the tokenizer
    std::string input;
    // expected output vector
    std::vector<Token> expected;
};

// Runs the tokenizer with a given input and checks that the output is correct.
void tokenizer_test(const TestParameters& params)
{
    std::istringstream ss(params.input);
    Tokenizer tokenizer(ss);
    std::vector<Token> tokens = tokenizer.tokenize();
    bool equal = tokens == params.expected;
    BOOST_CHECK(equal);
    if (!equal) {
        std::cout << "Input: " << params.input << "\n";
        std::cout << "Expected (" << params.expected.size() << "): ";
        for (const Token& token : params.expected) {
            std::cout << token << " ";
        }
        std::cout << "\nGot (" << tokens.size() << "): ";
        for (const Token& token : tokens) {
            std::cout << token << " ";
        }
        std::cout << "\n" << std::endl;
    }
}

test_suite *init_unit_test_suite(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

#define LEN(arr) (sizeof(arr) / sizeof(arr[0]))

    TestParameters params[] = {
        // no tokens at all
        {"", {}},
        // basic expression
        {"(+ 2 2)", {"(", "+", "2", "2", ")"}},
        // slightly more complex expression
        {"(let ((x y)))", {"(", "let", "(", "(", "x", "y", ")", ")", ")"}},
        // newline as separator
        {"(= a\nb)", {"(", "=", "a", "b", ")"}},
        // string
        {"(print \"hello world\")", {"(", "print", "\"hello world\"", ")"}},
        // string with escaped characters
        {"(print \"\\\"hi\\\"\")", {"(", "print", "\"\"hi\"\"", ")"}},
        // comment
        {"(* 2 ;comment\npi)", {"(", "*", "2", "pi", ")"}},
        // comment starting immediately after token
        {"(+ 2 3; 4\n)", {"(", "+", "2", "3", ")"}},
        // comment starting in middle of token
        {"(+ 2 const;ant\n)", {"(", "+", "2", "const", ")"}}
    };

    framework::master_test_suite()
        .add(BOOST_PARAM_TEST_CASE(tokenizer_test, params, params + LEN(params)));

    return nullptr;
}
