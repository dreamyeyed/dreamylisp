#include <sstream>

#define BOOST_TEST_MODULE parser
#include <boost/test/included/unit_test.hpp>

#include "parser.hpp"
#include "tokenizer.hpp"

using namespace dlisp;

BOOST_AUTO_TEST_CASE(simple_exprs)
{
    std::string code("(set! foo 1) (set! bar 2) (+ foo bar)");
    std::istringstream ss(code);

    Tokenizer tokenizer(ss);
    std::vector<Token> tokens = tokenizer.tokenize();

    Parser parser(tokens);
    Form nodes = parser.parse();

    BOOST_CHECK(nodes.size() == 3);
}

BOOST_AUTO_TEST_CASE(nested_exprs)
{
    std::string code(
        "(let ((x 1) (y 2))\n"
        "  (set! f\n"
        "        (fn (z)\n"
        "            (+ x y z))))"
    );
    std::istringstream ss(code);

    Tokenizer tokenizer(ss);
    std::vector<Token> tokens = tokenizer.tokenize();

    Parser parser(tokens);
    Form nodes = parser.parse();

    BOOST_CHECK(nodes.size() == 1);

    {
        Form *expr = boost::get<Form>(&(nodes[0]->body));
        BOOST_CHECK(expr != nullptr);
        BOOST_CHECK(expr->size() == 3);
    }
}
