A simple Lisp interpreter inspired by Peter Norvig's
[Lispy](http://norvig.com/lispy.html).
