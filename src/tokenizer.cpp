#include "tokenizer.hpp"

#include <cctype>

namespace
{
    enum class TokenizerState
    {
        NORMAL,
        IN_TOKEN,
        IN_STRING,
        IN_COMMENT
    };

    bool is_paren(char c) {
        return c == '(' || c == ')';
    }
}

namespace dlisp
{
    Tokenizer::Tokenizer(std::istream& is):
        is(is)
    {
    }

    std::vector<Token> Tokenizer::tokenize()
    {
        std::vector<Token> tokens;

        TokenizerState state = TokenizerState::NORMAL;
        Token curr_token;
        char c;
        while (is.get(c)) {
            switch (state) {
                case TokenizerState::NORMAL:
                if (std::isspace(c)) {
                    break;
                } else if (is_paren(c)) {
                    // parens are tokens by themselves
                    tokens.push_back(std::string() + c);
                } else if (c == ';') {
                    state = TokenizerState::IN_COMMENT;
                } else if (c == '"') {
                    curr_token += c;
                    state = TokenizerState::IN_STRING;
                } else {
                    state = TokenizerState::IN_TOKEN;
                    curr_token += c;
                }
                break;

                case TokenizerState::IN_TOKEN:
                if (std::isspace(c)) {
                    tokens.push_back(curr_token);
                    curr_token = "";
                    state = TokenizerState::NORMAL;
                } else if (is_paren(c)) {
                    tokens.push_back(curr_token);
                    curr_token = "";
                    is.unget();
                    state = TokenizerState::NORMAL;
                } else if (c == ';') {
                    tokens.push_back(curr_token);
                    curr_token = "";
                    state = TokenizerState::IN_COMMENT;
                } else {
                    curr_token += c;
                }
                break;

                case TokenizerState::IN_STRING:
                if (c == '\\') {
                    // escaped character
                    char next;
                    is.get(next);
                    if (next == '"') {
                        curr_token += '"';
                    } else if (next == '\\') {
                        curr_token += '\\';
                    } else if (next == 'n') {
                        curr_token += '\n';
                    } else if (next == 't') {
                        curr_token += '\t';
                    } else if (next == 'r') {
                        curr_token += '\r';
                    }
                } else if (c == '"') {
                    curr_token += c;
                    tokens.push_back(curr_token);
                    curr_token = "";
                    state = TokenizerState::NORMAL;
                } else {
                    curr_token += c;
                }
                break;

                case TokenizerState::IN_COMMENT:
                if (c == '\n') {
                    state = TokenizerState::NORMAL;
                }
                break;

                default:
                break;
            }
        }

        return tokens;
    }
}
