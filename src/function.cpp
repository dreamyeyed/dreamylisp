#include "eval.hpp"
#include "function.hpp"

#include <sstream>

namespace dlisp
{
    LispFunction::LispFunction(
        const std::shared_ptr<Env> env,
        const std::vector<Token>& param_names,
        const Form& body
    ):
        env(env),
        param_names(param_names),
        body(body)
    {

    }

    Value LispFunction::call(std::vector<Value>& args)
    {
        if (args.size() != param_names.size()) {
            std::ostringstream ss;
            ss << "Invalid number of args: expected " << param_names.size();
            ss << ", got " << args.size();
            throw InvalidFnCall(ss.str());
        }

        // Create a new Env for arguments.
        std::shared_ptr<Env> fn_call_env(new Env(this->env));
        for (size_t i = 0; i < args.size(); ++i) {
            fn_call_env->set(param_names[i], args[i]);
        }

        // Evaluate each expression in body and return the value of the last one.
        for (size_t i = 0; i < body.size() - 1; ++i) {
            eval(body, fn_call_env);
        }
        return eval(*body[body.size()-1], fn_call_env);
    }
}
