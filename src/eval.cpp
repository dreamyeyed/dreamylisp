#include "eval.hpp"

#include <cctype>
#include <sstream>

namespace {
    using namespace dlisp;

    class InvalidToken : public std::runtime_error
    {
        public:
        InvalidToken(const std::string& msg):
            runtime_error(msg)
        {
        }
    };

    // Converts a token to a Lisp value. Throws InvalidToken in case of error.
    Value to_value(const Token& token, const std::shared_ptr<Env> env)
    {
        if (token == "true") {
            return Value{true};
        } else if (token == "false") {
            return Value{false};
        } else if (token == "nil") {
            return Value{Nil{}};
        } else if (std::isdigit(token[0])) {
            size_t pos;
            double val = std::stod(token, &pos);
            // throw exception if there are extra characters
            if (pos != token.length()) {
                std::ostringstream ss;
                ss << "Invalid number: " << token;
                throw InvalidToken(ss.str());
            }
            return Value{val};
        } else if (token[0] == '"') {
            if (token[token.length() - 1] != '"') {
                // This is actually a parsing error
                std::ostringstream ss;
                ss << "Invalid string: " << token;
                throw InvalidToken(ss.str());
            }
            return Value{token.substr(1, token.length() - 2)};
        } else {
            return env->get(token);
        }
    }

    // Evaluates an if expression. Throws InvalidExpression in case of error.
    Value eval_if(const Form& form, std::shared_ptr<Env> env)
    {
        if (form.size() != 3 && form.size() != 4) {
            throw InvalidExpression(
                "If expression must be either\n"
                "(if cond expr) or\n"
                "(if cond true-expr false-expr)"
            );
        }

        Value cond = eval(*form[1], env);
        if (cond) {
            // true expression
            return eval(*form[2], env);
        } else if (form.size() == 4) {
            // false expression
            return eval(*form[3], env);
        } else {
            // nil because there's no false expression
            return Value{Nil{}};
        }
    }

    // Evaluates a variable definition. Throws InvalidExpression in case
    // of error.
    Value eval_def(const Form& form, std::shared_ptr<Env> env)
    {
        if (form.size() != 3) {
            throw InvalidExpression(
                "def expression must be (def symbol value)"
            );
        }

        Token *symbol;

        if ((symbol = boost::get<Token>(&form[1]->body)) == nullptr) {
            throw InvalidExpression("variable name must be a symbol");
        }

        Value value = eval(*form[2], env);
        env->set(*symbol, value);
        return value;
    }

    // Evaluates a function definition. Throws InvalidExpression in case
    // of error.
    Value eval_fn(const Form& form, std::shared_ptr<Env> env)
    {
        if (form.size() < 3) {
            throw InvalidExpression(
                "function definition must be (fn (args) expr1 ... exprn)"
            );
        }

        Form *param_list;
        if ((param_list = boost::get<Form>(&form[1]->body)) == nullptr) {
            throw InvalidExpression(
                "function definition must have a parameter list"
            );
        }

        std::vector<Token> param_names;
        for (const auto& param : *param_list) {
            if (Token *token = boost::get<Token>(&param->body)) {
                param_names.push_back(*token);
            } else {
                throw InvalidExpression("parameter list must contain symbols");
            }
        }

        Form fn_body;
        for (auto it = form.cbegin() + 2; it != form.cend(); ++it) {
            fn_body.push_back(*it);
        }

        return Value{LispFunction(env, param_names, fn_body)};
    }

    // Calls a C++ function.
    Value call_fn(CppFunction fn, const Form &form, std::shared_ptr<Env> env)
    {
        std::vector<Value> values;
        for (auto it = form.cbegin() + 1; it != form.cend(); ++it) {
            values.push_back(eval(**it, env));
        }
        return fn(values);
    }

    // Calls a Lisp function.
    Value call_fn(LispFunction& fn, const Form &form, std::shared_ptr<Env> env)
    {
        std::vector<Value> values;
        for (auto it = form.cbegin() + 1; it != form.cend(); ++it) {
            values.push_back(eval(**it, env));
        }
        return fn.call(values);
    }

    // Evaluates a Lisp form. Throws InvalidExpression in case of error.
    Value eval_form(const Form& form, std::shared_ptr<Env> env)
    {
        if (const Token *token = boost::get<Token>(&(form[0]->body))) {
            // Special form?
            if (*token == "if") {
                return eval_if(form, env);
            } else if (*token == "def") {
                return eval_def(form, env);
            } else if (*token == "fn") {
                return eval_fn(form, env);
            }

            // Normal function call
            Value val = env->get(*token);
            if (CppFunction *fn = boost::get<CppFunction>(&val.value)) {
                return call_fn(*fn, form, env);
            } else if (LispFunction *fn = boost::get<LispFunction>(&val.value)) {
                return call_fn(*fn, form, env);
            } else {
                std::ostringstream ss;
                ss << "Attempted to call non-function " << *token;
                throw InvalidExpression(ss.str());
            }
        }

        // TODO: allow form to begin with an expr that returns a function
        throw InvalidExpression("Form must begin with a function");
    }
}

namespace dlisp
{
    Value eval(const ASTNode& expr, std::shared_ptr<Env> env)
    {
        if (const Token *token = boost::get<Token>(&(expr.body))) {
            return to_value(*token, env);
        } else if (const Form *form = boost::get<Form>(&(expr.body))) {
            return eval_form(*form, env);
        } else {
            throw InvalidExpression("Couldn't parse expression");
        }
    }
}
