#include "value.hpp"

#include <sstream>

namespace dlisp
{
    Value::Value():
        value{Nil{}}
    {
    }

    Value::operator bool() const
    {
        if (const Nil *nil = boost::get<Nil>(&this->value)) {
            (void)nil; // we just want to know if it's a nil
            return false;
        } else if (const bool *b = boost::get<bool>(&this->value)) {
            return *b;
        } else {
            return true;
        }
    }

    Env::Env(std::shared_ptr<Env> parent):
        parent(parent)
    {
    }

    void Env::set(const std::string& name, const Value& value)
    {
        vars[name] = value;
    }

    Value Env::get(const std::string& name) const
    {
        if (vars.count(name) == 0) {
            if (parent) {
                return parent->get(name);
            } else {
                std::ostringstream ss;
                ss << "Invalid variable name " << name;
                throw NoSuchVar(ss.str());
            }
        }

        // must use .at() here because operator[] is not const
        return vars.at(name);
    }

    bool Env::exists(const std::string& name) const
    {
        return vars.count(name) > 0 || (parent && parent->exists(name));
    }
}
