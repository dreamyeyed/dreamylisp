#include "parser.hpp"

#include <sstream>

namespace
{
    using namespace dlisp;

    // Reads tokens starting at the iterator it. Continues until a complete
    // expression is returned (which is returned) or a parsing error happens.
    // Throws ParsingError if something goes wrong (such as mismatched parens).
    template<typename Iter>
    std::shared_ptr<ASTNode> parse_expr(Iter& it, const Iter& end)
    {
        Form nodes;
        
        while (it != end) {
            if (*it == "(") {
                nodes.push_back(parse_expr(++it, end));
            } else if (*it == ")") {
                ++it;
                return std::shared_ptr<ASTNode>(new ASTNode(nodes));
            } else {
                nodes.push_back(std::shared_ptr<ASTNode>(new ASTNode(*it)));
                ++it;
            }
        }

        throw ParsingError("Unclosed expression");
    }
}

namespace dlisp
{
    ASTNode::ASTNode(const Token& token):
        body(token)
    {
    }

    ASTNode::ASTNode(std::vector<std::shared_ptr<ASTNode>>& nodes):
        body(std::move(nodes))
    {
    }

    Parser::Parser(std::vector<Token>& tokens):
        tokens(std::move(tokens))
    {
    }

    Form Parser::parse() const
    {
        Form ast;

        auto it = tokens.cbegin();
        while (it != tokens.cend()) {
            if (*it != "(") {
                std::ostringstream ss;
                ss << "Error: token " << *it << "is not in an expression";
                throw ParsingError(ss.str());
            }
            ast.push_back(parse_expr(++it, tokens.cend()));
        }

        return ast;
    }
}
