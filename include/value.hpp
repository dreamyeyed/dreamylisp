/**
 * @file value.hpp
 * @author dreamyeyed
 *
 * Lisp values and related functions / classes.
 */

#pragma once

#include <memory>
#include <stdexcept>
#include <unordered_map>
#include <vector>

#include <boost/variant.hpp>

#include "function.hpp"

namespace dlisp
{
    // forward declarations
    struct Value;
    class LispFunction;

    /**
     * Function declared in C++ that can be called from Lisp.
     * (It's a typedef only because I haven't yet figured out how to use an
     * using statement for functions.) */
    typedef Value(*CppFunction)(const std::vector<Value>&);

    /** Represents lack of value. */
    struct Nil {};

    struct Value
    {
        Value();

        template<typename T>
        Value(T t):
            value{t}
        {
        }

        boost::variant<
            // Nil
            Nil,
            // Number
            double,
            // String
            std::string,
            // Boolean
            bool,
            // Function
            CppFunction,
            LispFunction
        > value;

        operator bool() const;
    };

    /**
     * Thrown when a non-existing variable is accessed.
     */
    class NoSuchVar : public std::runtime_error
    {
        public:
        NoSuchVar(const std::string& msg):
            runtime_error(msg)
        {
        }
    };

    /**
     * Env maintains a table of variables.
     */
    class Env
    {
        public:
        /**
         * Constructs a new Env.
         *
         * @param parent The new Env's parent. If a variable is not found in an
         * Env, its parent (and parent's parent, etc.) will also be searched.
         * Can be nullptr if there's no parent (e.g. the Env is the global Env).
         */
        Env(std::shared_ptr<Env> parent);

        /**
         * Sets a value in the Env.
         */
        void set(const std::string& name, const Value& value);

        /**
         * Gets a value from the Env. Throws NoSuchVar if the variable doesn't
         * exist.
         */
        Value get(const std::string& name) const;

        /**
         * Checks if a variable with the given name exists in the Env or any
         * of its parents.
         */
        bool exists(const std::string& name) const;

        private:
        std::unordered_map<std::string, Value> vars;
        std::shared_ptr<Env> parent;
    };
}
