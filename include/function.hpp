/**
 * @file function.hpp
 * @author dreamyeyed
 *
 * A class for functions written in Lisp.
 */

#pragma once

#include <stdexcept>

#include "parser.hpp"
#include "value.hpp"

namespace dlisp
{
    // forward declarations
    class Env;
    class Value;

    class InvalidFnCall : public std::runtime_error
    {
        public:
        InvalidFnCall(const std::string& msg):
            runtime_error(msg)
        {
        }
    };

    class LispFunction
    {
        public:
        /**
         * Constructor.
         *
         * @param env the environment where the function was defined
         * @param param_names names of the function parameters, used the create
         *        the environment for calling this function
         * @param body expressions of this function
         */
        LispFunction(
            std::shared_ptr<Env> env,
            const std::vector<Token>& param_names,
            const Form& body
        );

        /**
         * Calls the function with the given arguments. Throws InvalidFnCall
         * in case of error.
         */
        Value call(std::vector<Value>& args);

        private:
        std::shared_ptr<Env> env;
        std::vector<Token> param_names;
        Form body;
    };
}
