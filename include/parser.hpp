/**
 * @file parser.hpp
 * @author dreamyeyed
 *
 * Parser reads a vector of tokens and generates an abstract syntax tree.
 */

#pragma once

#include <memory>
#include <stdexcept>
#include <string>
#include <utility>

#include <boost/variant.hpp>

#include "tokenizer.hpp"

namespace dlisp
{
    class ParsingError : public std::runtime_error
    {
        public:
        ParsingError(const std::string& msg):
            runtime_error(msg)
        {
        }
    };

    /**
     * A node in the abstract syntax tree that represents the program.
     */
    struct ASTNode
    {
        ASTNode(const Token& token);
        ASTNode(std::vector<std::shared_ptr<ASTNode>>& nodes);

        boost::variant<Token, std::vector<std::shared_ptr<ASTNode>>> body;
    };

    using Form = std::vector<std::shared_ptr<ASTNode>>;

    class Parser
    {
        public:
        /**
         * Constructs the parser.
         *
         * @param tokens vector of tokens; note that it will be moved
         */
        Parser(std::vector<Token>& tokens);

        Form parse() const;

        private:
        std::vector<Token> tokens;
    };
}
