/**
 * @file tokenizer.hpp
 * @author dreamyeyed
 *
 * Reads tokens from an input stream.
 */

#pragma once

#include <istream>
#include <string>
#include <vector>

namespace dlisp
{
    using Token = std::string;

    /**
     * Splits input into tokens.
     */
    class Tokenizer
    {
        public:
        /**
         * Constructor. The input stream given as parameters must not be used
         * anywhere else or the tokenizer will work incorrectly.
         */
        Tokenizer(std::istream& is);

        /**
         * Returns a vector of tokens found in the input stream that was passed
         * to constructor.
         */
        std::vector<Token> tokenize();

        private:
        std::istream& is;
    };
}
