/**
 * @file eval.hpp
 * @author dreamyeyed
 *
 * Evaluates Lisp code.
 */

#pragma once

#include <stdexcept>
#include <string>

#include "parser.hpp"
#include "value.hpp"

namespace dlisp
{
    class InvalidExpression : public std::runtime_error
    {
        public:
        InvalidExpression(const std::string& msg):
            runtime_error(msg)
        {
        }
    };

    /**
     * Evaluates a Lisp expression and returns the result. Throws
     * InvalidExpression if there's something wrong with the expression.
     */
    Value eval(const ASTNode& expr, std::shared_ptr<Env> env);
}
