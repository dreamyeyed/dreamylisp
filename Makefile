SRC_FILES=$(wildcard src/*.cpp)
OBJ_FILES=$(addprefix obj/,$(notdir $(SRC_FILES:.cpp=.o)))
CXX=g++
FLAGS=-Wall -Wextra -Werror -std=c++14 -Iinclude -Llib
LIBS=-ldreamylisp

debug: FLAGS += -O0 -g -DDEBUG
release: FLAGS += -O2 -DNDEBUG

obj/%.o: src/%.cpp
	mkdir -p obj
	${CXX} ${FLAGS} -c -o $@ $<

lib/libdreamylisp.a: ${OBJ_FILES}
	mkdir -p lib
	ar rcs $@ $^

debug: lib/libdreamylisp.a
release: lib/libdreamylisp.a

TEST_SRC_FILES=$(wildcard tests/*.cpp)
TEST_EXE_FILES=$(addprefix tmp/tests/,$(basename $(notdir $(TEST_SRC_FILES))))

tmp/tests/%: tests/%.cpp lib/libdreamylisp.a
	mkdir -p tmp/tests
	${CXX} ${FLAGS} -O0 -g -DDEBUG -o $@ $< ${LIBS} 
	$@

tests: $(TEST_EXE_FILES)
	rm -rf tmp/tests

clean:
	rm obj/*.o
	rm lib/*.a
	rm -rf tmp
